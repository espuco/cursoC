﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Entidades;
//using Entidades;

namespace Negocio
{
    public class CN_Categoria
    {
        public static DataTable ListarCategoria(string codigo)
        {
            CD_Categorias Datos = new CD_Categorias();
            return Datos.ListarCategoria(codigo);
        }
        public static string GuardarCategoria(int opcion, E_Categoria categoria)
        {
            CD_Categorias Datos = new CD_Categorias();
            return Datos.GuardarCategoria(opcion, categoria);
        }

        public static string EliminarCategoria(int codigo)
        {
            CD_Categorias Datos = new CD_Categorias();
            return Datos.EliminarCategoria(codigo);
        }
    }
}
