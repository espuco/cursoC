﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class Frm_Principal : Form
    {
        Form1 MiPrimerFormulario;
        Frm_ComboBox combo;
        FrmIvaCheckBox IvaCheckBox;
        public Frm_Principal()
        {
            InitializeComponent();
        }

        private void comboBoxToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (combo == null)
            {
                combo = new Frm_ComboBox();
                combo.MdiParent = this;
                combo.FormClosed += new FormClosedEventHandler(altaFrmComboBox);
                combo.Show();
            }
            else
                combo.Activate();
        }
        void altaFrmComboBox(object sender, EventArgs e)
        {
            combo = null;
        }

        private void primerFormToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MiPrimerFormulario == null)
            {
                MiPrimerFormulario = new Form1();
                MiPrimerFormulario.MdiParent = this;
                MiPrimerFormulario.FormClosed += new FormClosedEventHandler(altaFrm1);
                MiPrimerFormulario.Show();
            }
            else
                MiPrimerFormulario.Activate();
        }
        void altaFrm1(object sender, EventArgs e)
        {
            MiPrimerFormulario = null;
        }

        private void calculosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (IvaCheckBox == null)
            {
                IvaCheckBox = new FrmIvaCheckBox();
                IvaCheckBox.MdiParent = this;
                IvaCheckBox.FormClosed += new FormClosedEventHandler(altaFrmComboBox);
                IvaCheckBox.Show();
            }
            else
                IvaCheckBox.Activate();            
        }

        void altaFrmIvaCheckBox(object sender, EventArgs e)
        {
            IvaCheckBox = null;
        }
    }
}
