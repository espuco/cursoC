﻿namespace Presentacion
{
    partial class Frm_Principal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menu_Opciones = new System.Windows.Forms.MenuStrip();
            this.datosMaestosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.procesosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sistemasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salirDelSistemaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.comboBoxToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tiempoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.numericUpDownToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.encuestaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataGridViewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.primerFormToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.calculosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Opciones.SuspendLayout();
            this.SuspendLayout();
            // 
            // menu_Opciones
            // 
            this.menu_Opciones.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.datosMaestosToolStripMenuItem,
            this.procesosToolStripMenuItem,
            this.reportesToolStripMenuItem,
            this.sistemasToolStripMenuItem,
            this.salirDelSistemaToolStripMenuItem});
            this.menu_Opciones.Location = new System.Drawing.Point(0, 0);
            this.menu_Opciones.Name = "menu_Opciones";
            this.menu_Opciones.Size = new System.Drawing.Size(800, 24);
            this.menu_Opciones.TabIndex = 1;
            this.menu_Opciones.Text = "menuStrip1";
            // 
            // datosMaestosToolStripMenuItem
            // 
            this.datosMaestosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.comboBoxToolStripMenuItem,
            this.tiempoToolStripMenuItem,
            this.numericUpDownToolStripMenuItem,
            this.encuestaToolStripMenuItem,
            this.dataGridViewToolStripMenuItem,
            this.primerFormToolStripMenuItem});
            this.datosMaestosToolStripMenuItem.Name = "datosMaestosToolStripMenuItem";
            this.datosMaestosToolStripMenuItem.Size = new System.Drawing.Size(96, 20);
            this.datosMaestosToolStripMenuItem.Text = "Datos Maestos";
            // 
            // procesosToolStripMenuItem
            // 
            this.procesosToolStripMenuItem.Name = "procesosToolStripMenuItem";
            this.procesosToolStripMenuItem.Size = new System.Drawing.Size(66, 20);
            this.procesosToolStripMenuItem.Text = "Procesos";
            // 
            // reportesToolStripMenuItem
            // 
            this.reportesToolStripMenuItem.Name = "reportesToolStripMenuItem";
            this.reportesToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.reportesToolStripMenuItem.Text = "Reportes";
            // 
            // sistemasToolStripMenuItem
            // 
            this.sistemasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.calculosToolStripMenuItem});
            this.sistemasToolStripMenuItem.Name = "sistemasToolStripMenuItem";
            this.sistemasToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.sistemasToolStripMenuItem.Text = "Sistemas";
            // 
            // salirDelSistemaToolStripMenuItem
            // 
            this.salirDelSistemaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.salirToolStripMenuItem});
            this.salirDelSistemaToolStripMenuItem.Name = "salirDelSistemaToolStripMenuItem";
            this.salirDelSistemaToolStripMenuItem.Size = new System.Drawing.Size(104, 20);
            this.salirDelSistemaToolStripMenuItem.Text = "Salir del Sistema";
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.salirToolStripMenuItem.Text = "Salir";
            // 
            // comboBoxToolStripMenuItem
            // 
            this.comboBoxToolStripMenuItem.Name = "comboBoxToolStripMenuItem";
            this.comboBoxToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.comboBoxToolStripMenuItem.Text = "ComboBox";
            this.comboBoxToolStripMenuItem.Click += new System.EventHandler(this.comboBoxToolStripMenuItem_Click);
            // 
            // tiempoToolStripMenuItem
            // 
            this.tiempoToolStripMenuItem.Name = "tiempoToolStripMenuItem";
            this.tiempoToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.tiempoToolStripMenuItem.Text = "Tiempo";
            // 
            // numericUpDownToolStripMenuItem
            // 
            this.numericUpDownToolStripMenuItem.Name = "numericUpDownToolStripMenuItem";
            this.numericUpDownToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.numericUpDownToolStripMenuItem.Text = "NumericUpDown";
            // 
            // encuestaToolStripMenuItem
            // 
            this.encuestaToolStripMenuItem.Name = "encuestaToolStripMenuItem";
            this.encuestaToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.encuestaToolStripMenuItem.Text = "Encuesta";
            // 
            // dataGridViewToolStripMenuItem
            // 
            this.dataGridViewToolStripMenuItem.Name = "dataGridViewToolStripMenuItem";
            this.dataGridViewToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.dataGridViewToolStripMenuItem.Text = "DataGridView";
            // 
            // primerFormToolStripMenuItem
            // 
            this.primerFormToolStripMenuItem.Name = "primerFormToolStripMenuItem";
            this.primerFormToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.primerFormToolStripMenuItem.Text = "Primer Form";
            this.primerFormToolStripMenuItem.Click += new System.EventHandler(this.primerFormToolStripMenuItem_Click);
            // 
            // calculosToolStripMenuItem
            // 
            this.calculosToolStripMenuItem.Name = "calculosToolStripMenuItem";
            this.calculosToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.calculosToolStripMenuItem.Text = "Calculos";
            this.calculosToolStripMenuItem.Click += new System.EventHandler(this.calculosToolStripMenuItem_Click);
            // 
            // Frm_Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.menu_Opciones);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menu_Opciones;
            this.Name = "Frm_Principal";
            this.Text = "Contenedor Principal";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.menu_Opciones.ResumeLayout(false);
            this.menu_Opciones.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menu_Opciones;
        private System.Windows.Forms.ToolStripMenuItem datosMaestosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem comboBoxToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tiempoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem numericUpDownToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem encuestaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dataGridViewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem procesosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sistemasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salirDelSistemaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem primerFormToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem calculosToolStripMenuItem;
    }
}