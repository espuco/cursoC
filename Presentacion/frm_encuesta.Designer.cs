﻿namespace Presentacion
{
    partial class frm_encuesta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ch_foxPro = new System.Windows.Forms.CheckBox();
            this.ch_C = new System.Windows.Forms.CheckBox();
            this.ch_visualBasic = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ch_java = new System.Windows.Forms.CheckBox();
            this.txt_resultado = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_procesar = new System.Windows.Forms.Button();
            this.rb_presencial = new System.Windows.Forms.RadioButton();
            this.rb_virtual = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // ch_foxPro
            // 
            this.ch_foxPro.AutoSize = true;
            this.ch_foxPro.Location = new System.Drawing.Point(151, 70);
            this.ch_foxPro.Name = "ch_foxPro";
            this.ch_foxPro.Size = new System.Drawing.Size(93, 17);
            this.ch_foxPro.TabIndex = 0;
            this.ch_foxPro.Text = "Visual Fox Pro";
            this.ch_foxPro.UseVisualStyleBackColor = true;
            // 
            // ch_C
            // 
            this.ch_C.AutoSize = true;
            this.ch_C.Location = new System.Drawing.Point(151, 93);
            this.ch_C.Name = "ch_C";
            this.ch_C.Size = new System.Drawing.Size(43, 17);
            this.ch_C.TabIndex = 1;
            this.ch_C.Text = "C #";
            this.ch_C.UseVisualStyleBackColor = true;
            // 
            // ch_visualBasic
            // 
            this.ch_visualBasic.AutoSize = true;
            this.ch_visualBasic.Location = new System.Drawing.Point(151, 116);
            this.ch_visualBasic.Name = "ch_visualBasic";
            this.ch_visualBasic.Size = new System.Drawing.Size(58, 17);
            this.ch_visualBasic.TabIndex = 2;
            this.ch_visualBasic.Text = "VB.net";
            this.ch_visualBasic.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Maroon;
            this.label1.Location = new System.Drawing.Point(98, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(161, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Cursos disponibles en promoción";
            // 
            // ch_java
            // 
            this.ch_java.AutoSize = true;
            this.ch_java.Location = new System.Drawing.Point(151, 139);
            this.ch_java.Name = "ch_java";
            this.ch_java.Size = new System.Drawing.Size(49, 17);
            this.ch_java.TabIndex = 4;
            this.ch_java.Text = "Java";
            this.ch_java.UseVisualStyleBackColor = true;
            // 
            // txt_resultado
            // 
            this.txt_resultado.Location = new System.Drawing.Point(404, 191);
            this.txt_resultado.Multiline = true;
            this.txt_resultado.Name = "txt_resultado";
            this.txt_resultado.ReadOnly = true;
            this.txt_resultado.Size = new System.Drawing.Size(253, 122);
            this.txt_resultado.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(401, 168);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(172, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Resultado de textos seleccionados";
            // 
            // btn_procesar
            // 
            this.btn_procesar.Location = new System.Drawing.Point(129, 191);
            this.btn_procesar.Name = "btn_procesar";
            this.btn_procesar.Size = new System.Drawing.Size(75, 23);
            this.btn_procesar.TabIndex = 7;
            this.btn_procesar.Text = "Procesar";
            this.btn_procesar.UseVisualStyleBackColor = true;
            this.btn_procesar.Click += new System.EventHandler(this.btn_procesar_Click);
            // 
            // rb_presencial
            // 
            this.rb_presencial.AutoSize = true;
            this.rb_presencial.Location = new System.Drawing.Point(418, 127);
            this.rb_presencial.Name = "rb_presencial";
            this.rb_presencial.Size = new System.Drawing.Size(74, 17);
            this.rb_presencial.TabIndex = 8;
            this.rb_presencial.TabStop = true;
            this.rb_presencial.Text = "Presencial";
            this.rb_presencial.UseVisualStyleBackColor = true;
            // 
            // rb_virtual
            // 
            this.rb_virtual.AutoSize = true;
            this.rb_virtual.Location = new System.Drawing.Point(518, 127);
            this.rb_virtual.Name = "rb_virtual";
            this.rb_virtual.Size = new System.Drawing.Size(54, 17);
            this.rb_virtual.TabIndex = 9;
            this.rb_virtual.TabStop = true;
            this.rb_virtual.Text = "Virtual";
            this.rb_virtual.UseVisualStyleBackColor = true;
            // 
            // frm_encuesta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.rb_virtual);
            this.Controls.Add(this.rb_presencial);
            this.Controls.Add(this.btn_procesar);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txt_resultado);
            this.Controls.Add(this.ch_java);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ch_visualBasic);
            this.Controls.Add(this.ch_C);
            this.Controls.Add(this.ch_foxPro);
            this.Name = "frm_encuesta";
            this.Text = "Form2";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox ch_foxPro;
        private System.Windows.Forms.CheckBox ch_C;
        private System.Windows.Forms.CheckBox ch_visualBasic;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox ch_java;
        private System.Windows.Forms.TextBox txt_resultado;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_procesar;
        private System.Windows.Forms.RadioButton rb_presencial;
        private System.Windows.Forms.RadioButton rb_virtual;
    }
}