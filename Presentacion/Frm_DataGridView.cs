﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class Frm_DataGridView : Form
    {
        public Frm_DataGridView()
        {
            InitializeComponent();
        }

        private void btn_Agregar_Click(object sender, EventArgs e)
        {
            if (txt_nombre.Text!="" &&txt_email.Text!="")
            {
                dgv_datos.Rows.Add(txt_nombre.Text, txt_email.Text);
                txt_nombre.Text = "";
                txt_email.Text = "";
                MessageBox.Show("Datos agregados");
            }
        }

        private void btn_Eliminar_Click(object sender, EventArgs e)
        {
            //int NumeroFila;
            //NumeroFila;
            DialogResult Respuesta;
            Respuesta = MessageBox.Show("Seguro de eliminar?","Aviso del sistema",MessageBoxButtons.YesNoCancel);
            if (Respuesta==DialogResult.Yes)
            {
                dgv_datos.Rows.RemoveAt(dgv_datos.CurrentRow.Index);
                MessageBox.Show("Datos eliminados");
            }
            
        }

        private void dgv_datos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txt_nombre.Text = Convert.ToString(dgv_datos.CurrentRow.Cells["Column1"].Value);
            txt_email.Text = Convert.ToString(dgv_datos.CurrentRow.Cells["Column2"].Value);
        }
    }
}
