﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class FrmIvaCheckBox : Form
    {
        int UnidadDeManejo;
        double PrecioPorUnidadCorporacionFavorita;
        double PrecioPorUnidadHoreca;
        double PrecioPorUnidadEmpleado;

        double tieneIvaCorporacion = 0;
        double tieneIvaHoreca = 0;
        double tieneIvaEmpleado = 0;


        public FrmIvaCheckBox()
        {
            InitializeComponent();
        }
        private void FrmIvaCheckBox_Load(object sender, EventArgs e)
        {
            CargarDatos();
            UnidadDeManejo = Convert.ToInt32(txt_UnidadDeManejo.Text);
            PrecioPorUnidadCorporacionFavorita = Convert.ToDouble(txt_PrecioPorUnidadCorporacionFavorita.Text);
            PrecioPorUnidadHoreca = Convert.ToDouble(txt_PrecioPorUnidadHoreca.Text);
        }

        private void CargarDatos()
        {
            txt_UnidadDeManejo.Text = "180";
            txt_PrecioPorUnidadCorporacionFavorita.Text = "0.2300";
            txt_PrecioPorUnidadHoreca.Text = "0.2900";
            txt_PrecioPorUnidadEmpleado.Text = "0.2300";
        }

        private void cmb_SistemaDeFacturacion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmb_SistemaDeFacturacion.Text == "")
            {
                LimpiarTodo();
            }
            if (cmb_SistemaDeFacturacion.Text == "Jaba")
            {
                LimpiarTodo();
                chb_TieneIva.Checked = false;
                chb_JabaCajaCorporacionFavorita.Checked = true;
                chb_JabaCajaHoreca.Checked = true;
                chb_UnidadEmpleado.Checked = false;
                chb_UnidadEmpleado.Enabled = false;
                CalcularIvaCorporacionFavorita(UnidadDeManejo, PrecioPorUnidadCorporacionFavorita, false);
                CalcularIvaHoreca(UnidadDeManejo, PrecioPorUnidadHoreca, false);
                LimpiarEmpleado();
            }
            if (cmb_SistemaDeFacturacion.Text == "Caja")
            {
                LimpiarTodo();
                chb_TieneIva.Checked = false;
                chb_JabaCajaCorporacionFavorita.Checked = true;
                chb_JabaCajaHoreca.Checked = true;
                chb_UnidadEmpleado.Checked = false;
                chb_UnidadEmpleado.Enabled = false;
                CalcularIvaCorporacionFavorita(UnidadDeManejo, PrecioPorUnidadCorporacionFavorita, false);
                CalcularIvaHoreca(UnidadDeManejo, PrecioPorUnidadHoreca, false);
                LimpiarEmpleado();
            }
            if (cmb_SistemaDeFacturacion.Text == "Unidad")
            {
                LimpiarTodo();
                chb_TieneIva.Checked = false;
                chb_JabaCajaCorporacionFavorita.Checked = false;
                chb_JabaCajaHoreca.Checked = false;
                chb_UnidadEmpleado.Enabled = true;
                chb_UnidadEmpleado.Checked = true;
                CalcularIvaEmpleado(UnidadDeManejo, PrecioPorUnidadEmpleado, false);
                LimpiarCorporacionFavoritaHoreca();
            }
        }

        private void chb_TieneIva_CheckedChanged(object sender, EventArgs e)
        {
            if (chb_TieneIva.Checked == true)
            {
                //corporación favorita
                txt_TieneIvaCorporacionFavorita.Visible = true;
                tieneIvaCorporacion = Convert.ToDouble(txt_PrecioPorUnidadCorporacionFavorita.Text) * 12 / 100;
                txt_TieneIvaCorporacionFavorita.Text = Convert.ToString(tieneIvaCorporacion);
                CalcularIvaCorporacionFavorita(UnidadDeManejo, PrecioPorUnidadCorporacionFavorita, true);
                //horeca                
                txt_TieneIvaHoreca.Visible = true;
                tieneIvaHoreca = Convert.ToDouble(txt_PrecioPorUnidadHoreca.Text) * 12 / 100;
                txt_TieneIvaHoreca.Text = Convert.ToString(tieneIvaHoreca);
                CalcularIvaHoreca(UnidadDeManejo, PrecioPorUnidadHoreca, true);
                //Empleado
                txt_TieneIvaEmpleado.Visible = true;
                tieneIvaEmpleado = Convert.ToDouble(txt_PrecioPorUnidadEmpleado.Text) * 12 / 100;
                txt_TieneIvaEmpleado.Text = Convert.ToString(tieneIvaEmpleado);
                CalcularIvaEmpleado(UnidadDeManejo, PrecioPorUnidadEmpleado, true);
            }
            else
            {
                //corporación favorita
                txt_TieneIvaCorporacionFavorita.Visible = false;
                txt_TieneIvaCorporacionFavorita.Text = "0";
                tieneIvaCorporacion = 0;
                txt_TieneIvaCorporacionFavorita.Text = Convert.ToString(tieneIvaCorporacion);
                CalcularIvaCorporacionFavorita(UnidadDeManejo, PrecioPorUnidadCorporacionFavorita, false);
                //horeca
                txt_TieneIvaHoreca.Visible = false;
                txt_TieneIvaHoreca.Text = "0";
                tieneIvaHoreca = 0;
                txt_TieneIvaHoreca.Text = Convert.ToString(tieneIvaHoreca);
                CalcularIvaHoreca(UnidadDeManejo, PrecioPorUnidadHoreca, false);
                //empleado
                txt_TieneIvaEmpleado.Visible = false;
                txt_TieneIvaEmpleado.Text = "0";
                tieneIvaEmpleado = 0;
                txt_TieneIvaEmpleado.Text = Convert.ToString(tieneIvaEmpleado);
                CalcularIvaEmpleado(UnidadDeManejo, PrecioPorUnidadEmpleado, false);
            }
            if (chb_TieneIva.Checked == true && cmb_SistemaDeFacturacion.Text == "")
            {
                LimpiarTodo();
            }
            else if (chb_TieneIva.Checked == false && cmb_SistemaDeFacturacion.Text == "")
            {
                LimpiarTodo();
            }
            else if (chb_TieneIva.Checked == true && cmb_SistemaDeFacturacion.Text == "Jaba" || chb_TieneIva.Checked == false && cmb_SistemaDeFacturacion.Text == "Jaba")
            {
                LimpiarEmpleado();
            }
            else if (chb_TieneIva.Checked == true && cmb_SistemaDeFacturacion.Text == "Caja" || chb_TieneIva.Checked == false && cmb_SistemaDeFacturacion.Text == "Caja")
            {
                LimpiarEmpleado();
            }
            else if (chb_TieneIva.Checked == true && cmb_SistemaDeFacturacion.Text == "Unidad" || chb_TieneIva.Checked == false && cmb_SistemaDeFacturacion.Text == "Unidad")
            {
                LimpiarCorporacionFavoritaHoreca();
            }
            //else if (chb_TieneIva.Checked == true && chb_UnidadEmpleado.Checked == false && cmb_SistemaDeFacturacion.Text == "Unidad")
            //{
            //    LimpiarTodo();
            //}
            else if (chb_TieneIva.Checked == true && chb_UnidadEmpleado.Checked == true && cmb_SistemaDeFacturacion.Text == "Unidad")
            {
                tieneIvaEmpleado = Convert.ToDouble(txt_PrecioPorUnidadEmpleado.Text) * 12 / 100;
                txt_TieneIvaEmpleado.Text = Convert.ToString(tieneIvaEmpleado);
                CalcularIvaEmpleado(UnidadDeManejo, PrecioPorUnidadEmpleado, false);
            }
        }
        private void chb_UnidadEmpleado_CheckedChanged(object sender, EventArgs e)
        {
            if (chb_UnidadEmpleado.Checked == false)
            {
                LimpiarEmpleado();
                chb_TieneIva.Checked = false;
            }
            else
            {
                CalcularIvaEmpleado(UnidadDeManejo, PrecioPorUnidadEmpleado, false);
            }
            if (chb_TieneIva.Checked == false && chb_UnidadEmpleado.Checked == false && cmb_SistemaDeFacturacion.Text == "Unidad")
            {
                cmb_SistemaDeFacturacion.Text = "";//LimpiarEmpleado();
            }
            else if (chb_TieneIva.Checked == true && chb_UnidadEmpleado.Checked == false && cmb_SistemaDeFacturacion.Text == "Unidad")
            {
                chb_TieneIva.Checked = false;
                cmb_SistemaDeFacturacion.Text = "";
            }
            else if (chb_TieneIva.Checked == true && chb_UnidadEmpleado.Checked == true && cmb_SistemaDeFacturacion.Text == "Unidad")
            {
                tieneIvaEmpleado = Convert.ToDouble(txt_PrecioPorUnidadEmpleado.Text) * 12 / 100;
                txt_TieneIvaEmpleado.Text = Convert.ToString(tieneIvaEmpleado);
                CalcularIvaEmpleado(UnidadDeManejo, PrecioPorUnidadEmpleado, false);
            }
            if (chb_TieneIva.Checked == true && chb_UnidadEmpleado.Checked == false)
            {
                LimpiarTodo();
            }
        }

        private void LimpiarEmpleado()
        {
            txt_TieneIvaEmpleado.Text = "0.00";
            txt_PrecioPorUnidadFinalEmpleado.Text = "0.00";
            txt_PrecioPorUnidadManejoEmpleado.Text = "0.00";
            txt_IvaEmpleado.Text = "0.00";
            txt_PrecioFinalEmpleado.Text = "0.00";
        }
        private void LimpiarCorporacionFavoritaHoreca()
        {
            txt_TieneIvaCorporacionFavorita.Text = "0.00";
            txt_PrecioPorUnidadFinalCorporacionFavorita.Text = "0.00";
            txt_PrecioPorUnidadManejoCorporacionFavorita.Text = "0.00";
            txt_IvaCorporacionFavorita.Text = "0.00";
            txt_PrecioFinalCorporacionFavorita.Text = "0.00";

            txt_TieneIvaHoreca.Text = "0.00";
            txt_PrecioPorUnidadFinalHoreca.Text = "0.00";
            txt_PrecioPorUnidadManejoHoreca.Text = "0.00";
            txt_IvaHoreca.Text = "0.00";
            txt_PrecioFinalHoreca.Text = "0.00";
        }
        private void LimpiarTodo()
        {
            txt_TieneIvaCorporacionFavorita.Text = "0.00";
            txt_PrecioPorUnidadFinalCorporacionFavorita.Text = "0.00";
            txt_PrecioPorUnidadManejoCorporacionFavorita.Text = "0.00";
            txt_IvaCorporacionFavorita.Text = "0.00";
            txt_PrecioFinalCorporacionFavorita.Text = "0.00";

            txt_TieneIvaHoreca.Text = "0.00";
            txt_PrecioPorUnidadFinalHoreca.Text = "0.00";
            txt_PrecioPorUnidadManejoHoreca.Text = "0.00";
            txt_IvaHoreca.Text = "0.00";
            txt_PrecioFinalHoreca.Text = "0.00";

            txt_TieneIvaEmpleado.Text = "0.00";
            txt_PrecioPorUnidadFinalEmpleado.Text = "0.00";
            txt_PrecioPorUnidadManejoEmpleado.Text = "0.00";
            txt_IvaEmpleado.Text = "0.00";
            txt_PrecioFinalEmpleado.Text = "0.00";
        }



        private void CalcularIvaCorporacionFavorita(int UnidadDeManejo, double Precio, bool IvaParam)
        {
            UnidadDeManejo = Convert.ToInt32(txt_UnidadDeManejo.Text);
            Precio = Convert.ToDouble(txt_PrecioPorUnidadCorporacionFavorita.Text);
            if (IvaParam == true)
            {
                double Iva = tieneIvaCorporacion;
                txt_TieneIvaCorporacionFavorita.Text = Convert.ToString(tieneIvaCorporacion);
                double PrecioPorUnidadFinal = Precio + Iva;
                txt_PrecioPorUnidadFinalCorporacionFavorita.Text = Convert.ToString(PrecioPorUnidadFinal);
                double PrecioPorUnidadManejo = PrecioPorUnidadFinal * UnidadDeManejo;
                txt_PrecioPorUnidadManejoCorporacionFavorita.Text = Convert.ToString(PrecioPorUnidadManejo);
                double IvaUnidadManejo = PrecioPorUnidadManejo * 12 / 100;
                txt_IvaCorporacionFavorita.Text = Convert.ToString(IvaUnidadManejo);
                double PrecioFinal = PrecioPorUnidadManejo + IvaUnidadManejo;
                txt_PrecioFinalCorporacionFavorita.Text = Convert.ToString(PrecioFinal);
            }
            else
            {
                double Iva = Precio * 0;
                txt_TieneIvaCorporacionFavorita.Text = Convert.ToString(Iva);
                double PrecioPorUnidadFinal = Precio + Iva;
                txt_PrecioPorUnidadFinalCorporacionFavorita.Text = Convert.ToString(PrecioPorUnidadFinal);
                double PrecioPorUnidadManejo = PrecioPorUnidadFinal * UnidadDeManejo;
                txt_PrecioPorUnidadManejoCorporacionFavorita.Text = Convert.ToString(PrecioPorUnidadManejo);
                double IvaUnidadManejo = PrecioPorUnidadManejo * 0;
                txt_IvaCorporacionFavorita.Text = Convert.ToString(IvaUnidadManejo);
                double PrecioFinal = PrecioPorUnidadManejo + IvaUnidadManejo;
                txt_PrecioFinalCorporacionFavorita.Text = Convert.ToString(PrecioFinal);
            }
        }

        private void CalcularIvaHoreca(int UnidadDeManejo, double Precio, bool IvaParam)
        {
            UnidadDeManejo = Convert.ToInt32(txt_UnidadDeManejo.Text);
            Precio = Convert.ToDouble(txt_PrecioPorUnidadHoreca.Text);
            if (IvaParam == true)
            {
                double Iva = tieneIvaHoreca;
                txt_TieneIvaHoreca.Text = Convert.ToString(tieneIvaHoreca);
                double PrecioPorUnidadFinal = Precio + Iva;
                txt_PrecioPorUnidadFinalHoreca.Text = Convert.ToString(PrecioPorUnidadFinal);
                double PrecioPorUnidadManejo = PrecioPorUnidadFinal * UnidadDeManejo;
                txt_PrecioPorUnidadManejoHoreca.Text = Convert.ToString(PrecioPorUnidadManejo);
                double IvaUnidadManejo = PrecioPorUnidadManejo * 12 / 100;
                txt_IvaHoreca.Text = Convert.ToString(IvaUnidadManejo);
                double PrecioFinal = PrecioPorUnidadManejo + IvaUnidadManejo;
                txt_PrecioFinalHoreca.Text = Convert.ToString(PrecioFinal);
            }
            else
            {
                double Iva = Precio * 0;
                txt_TieneIvaHoreca.Text = Convert.ToString(Iva);
                double PrecioPorUnidadFinal = Precio + Iva;
                txt_PrecioPorUnidadFinalHoreca.Text = Convert.ToString(PrecioPorUnidadFinal);
                double PrecioPorUnidadManejo = PrecioPorUnidadFinal * UnidadDeManejo;
                txt_PrecioPorUnidadManejoHoreca.Text = Convert.ToString(PrecioPorUnidadManejo);
                double IvaUnidadManejo = PrecioPorUnidadManejo * 0;
                txt_IvaHoreca.Text = Convert.ToString(IvaUnidadManejo);
                double PrecioFinal = PrecioPorUnidadManejo + IvaUnidadManejo;
                txt_PrecioFinalHoreca.Text = Convert.ToString(PrecioFinal);
            }
        }

        private void CalcularIvaEmpleado(int UnidadDeManejo, double Precio, bool IvaParam)
        {
            UnidadDeManejo = Convert.ToInt32(txt_UnidadDeManejo.Text);
            Precio = Convert.ToDouble(txt_PrecioPorUnidadEmpleado.Text);
            if (IvaParam == true)
            {
                double Iva = tieneIvaEmpleado;
                txt_TieneIvaEmpleado.Text = Convert.ToString(tieneIvaEmpleado);
                double PrecioPorUnidadFinal = Precio + Iva;
                txt_PrecioPorUnidadFinalEmpleado.Text = Convert.ToString(PrecioPorUnidadFinal);
                double PrecioPorUnidadManejo = PrecioPorUnidadFinal * UnidadDeManejo;
                txt_PrecioPorUnidadManejoEmpleado.Text = Convert.ToString(PrecioPorUnidadManejo);
                double IvaUnidadManejo = PrecioPorUnidadManejo * 12 / 100;
                txt_IvaEmpleado.Text = Convert.ToString(IvaUnidadManejo);
                double PrecioFinal = PrecioPorUnidadManejo + IvaUnidadManejo;
                txt_PrecioFinalEmpleado.Text = Convert.ToString(PrecioFinal);
            }
            else
            {
                double Iva = Precio * 0;
                txt_TieneIvaEmpleado.Text = Convert.ToString(Iva);
                double PrecioPorUnidadFinal = Precio + Iva;
                txt_PrecioPorUnidadFinalEmpleado.Text = Convert.ToString(PrecioPorUnidadFinal);
                double PrecioPorUnidadManejo = PrecioPorUnidadFinal * UnidadDeManejo;
                txt_PrecioPorUnidadManejoEmpleado.Text = Convert.ToString(PrecioPorUnidadManejo);
                double IvaUnidadManejo = PrecioPorUnidadManejo * 0;
                txt_IvaEmpleado.Text = Convert.ToString(IvaUnidadManejo);
                double PrecioFinal = PrecioPorUnidadManejo + IvaUnidadManejo;
                txt_PrecioFinalEmpleado.Text = Convert.ToString(PrecioFinal);
            }
        }

    }
}
