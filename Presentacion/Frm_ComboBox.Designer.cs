﻿namespace Presentacion
{
    partial class Frm_ComboBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txt_Nuevo = new System.Windows.Forms.TextBox();
            this.btn_Registrar = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_Resultado = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cmb_CursoSeleccionado = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Curso nuevo";
            // 
            // txt_Nuevo
            // 
            this.txt_Nuevo.Location = new System.Drawing.Point(85, 41);
            this.txt_Nuevo.Name = "txt_Nuevo";
            this.txt_Nuevo.Size = new System.Drawing.Size(100, 20);
            this.txt_Nuevo.TabIndex = 1;
            // 
            // btn_Registrar
            // 
            this.btn_Registrar.Location = new System.Drawing.Point(91, 112);
            this.btn_Registrar.Name = "btn_Registrar";
            this.btn_Registrar.Size = new System.Drawing.Size(75, 23);
            this.btn_Registrar.TabIndex = 2;
            this.btn_Registrar.Text = "Registrar";
            this.btn_Registrar.UseVisualStyleBackColor = true;
            this.btn_Registrar.Click += new System.EventHandler(this.btn_Registrar_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(354, 122);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Curso seleccionado";
            // 
            // txt_Resultado
            // 
            this.txt_Resultado.Location = new System.Drawing.Point(357, 153);
            this.txt_Resultado.Multiline = true;
            this.txt_Resultado.Name = "txt_Resultado";
            this.txt_Resultado.ReadOnly = true;
            this.txt_Resultado.Size = new System.Drawing.Size(187, 65);
            this.txt_Resultado.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(314, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Cursos disponibles";
            // 
            // cmb_CursoSeleccionado
            // 
            this.cmb_CursoSeleccionado.FormattingEnabled = true;
            this.cmb_CursoSeleccionado.Items.AddRange(new object[] {
            "Fox Pro",
            ".NET",
            "Java"});
            this.cmb_CursoSeleccionado.Location = new System.Drawing.Point(423, 44);
            this.cmb_CursoSeleccionado.Name = "cmb_CursoSeleccionado";
            this.cmb_CursoSeleccionado.Size = new System.Drawing.Size(121, 21);
            this.cmb_CursoSeleccionado.TabIndex = 6;
            this.cmb_CursoSeleccionado.Text = "Seleccione un curso";
            this.cmb_CursoSeleccionado.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // Frm_ComboBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.cmb_CursoSeleccionado);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txt_Resultado);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btn_Registrar);
            this.Controls.Add(this.txt_Nuevo);
            this.Controls.Add(this.label1);
            this.Name = "Frm_ComboBox";
            this.Text = "Frm_ComboBox";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_Nuevo;
        private System.Windows.Forms.Button btn_Registrar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_Resultado;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmb_CursoSeleccionado;
    }
}