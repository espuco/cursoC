﻿namespace Presentacion
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nud_Dia = new System.Windows.Forms.NumericUpDown();
            this.txt_resultado = new System.Windows.Forms.TextBox();
            this.btn_Dia = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.nud_Dia)).BeginInit();
            this.SuspendLayout();
            // 
            // nud_Dia
            // 
            this.nud_Dia.Location = new System.Drawing.Point(12, 21);
            this.nud_Dia.Maximum = new decimal(new int[] {
            7,
            0,
            0,
            0});
            this.nud_Dia.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nud_Dia.Name = "nud_Dia";
            this.nud_Dia.ReadOnly = true;
            this.nud_Dia.Size = new System.Drawing.Size(120, 20);
            this.nud_Dia.TabIndex = 0;
            this.nud_Dia.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // txt_resultado
            // 
            this.txt_resultado.Location = new System.Drawing.Point(138, 20);
            this.txt_resultado.Name = "txt_resultado";
            this.txt_resultado.ReadOnly = true;
            this.txt_resultado.Size = new System.Drawing.Size(100, 20);
            this.txt_resultado.TabIndex = 1;
            // 
            // btn_Dia
            // 
            this.btn_Dia.Location = new System.Drawing.Point(88, 47);
            this.btn_Dia.Name = "btn_Dia";
            this.btn_Dia.Size = new System.Drawing.Size(75, 23);
            this.btn_Dia.TabIndex = 2;
            this.btn_Dia.Text = "Mostrar día";
            this.btn_Dia.UseVisualStyleBackColor = true;
            this.btn_Dia.Click += new System.EventHandler(this.btn_Dia_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(256, 110);
            this.Controls.Add(this.btn_Dia);
            this.Controls.Add(this.txt_resultado);
            this.Controls.Add(this.nud_Dia);
            this.Name = "Form2";
            this.Text = "Form2";
            ((System.ComponentModel.ISupportInitialize)(this.nud_Dia)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown nud_Dia;
        private System.Windows.Forms.TextBox txt_resultado;
        private System.Windows.Forms.Button btn_Dia;
    }
}