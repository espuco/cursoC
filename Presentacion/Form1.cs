﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class Form1 : Form
    {
        //variables
        int estadoGuardar = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_nuevo_Click(object sender, EventArgs e)
        {
            estadoGuardar = 1;//nuevo registro
            lst_mantenimiento.Enabled = false;
            //limpiar caja de texto
            txt_codigo.Text = "";
            txt_descripcion.Text = "";

            //txt_codigo.Enabled = true;
            //txt_descripcion.Enabled = true;
            grb_mantenimiento.Enabled = true;
            txt_codigo.Enabled = true;
            txt_codigo.Focus();

            //btn_cancelar.Visible = true;
            //btn_guardar.Visible = true;

            //btn_nuevo.Enabled = false;
            //btn_actualizar.Visible = false;
            //btn_eliminar.Visible = false;
            //btn_reporte.Enabled = false;
            //btn_salir.Enabled = false;
            grb_botonesPrincipales.Enabled = false;
        }

        private void btn_cancelar_Click(object sender, EventArgs e)
        {
            txt_codigo.Text = "";
            txt_descripcion.Text = "";

            //txt_codigo.Enabled = false;
            //txt_descripcion.Enabled = false;

            //btn_cancelar.Visible = false;
            //btn_guardar.Visible = false;
            grb_mantenimiento.Enabled = false;

            //btn_nuevo.Enabled = true;
            //btn_actualizar.Visible = true;
            //btn_eliminar.Visible = true;
            //btn_reporte.Enabled = true;
            //btn_salir.Enabled = true;
            grb_botonesPrincipales.Enabled = true;
            lst_mantenimiento.Enabled = true;
        }

        private void btn_guardar_Click(object sender, EventArgs e)
        {
            string registro;
            registro = txt_codigo.Text.Trim() + " "+txt_descripcion.Text.Trim();

            if (estadoGuardar==1)//nuevo registro
            {
                lst_mantenimiento.Items.Add(registro);
            }
            else
            {
                int elementoSeleccinado = lst_mantenimiento.SelectedIndex;
                lst_mantenimiento.Items.Remove(lst_mantenimiento.SelectedItem);
                lst_mantenimiento.Items.Insert(elementoSeleccinado, registro);
            }
            
            MessageBox.Show("Datos guardados");
            Limpiar();
            lst_mantenimiento.Enabled = true;
        }

        private void Limpiar()
        {
            txt_codigo.Text = "";
            txt_descripcion.Text = "";
            grb_mantenimiento.Enabled = false;
            grb_botonesPrincipales.Enabled = true;
        }

        private void btn_eliminar_Click(object sender, EventArgs e)
        {
            lst_mantenimiento.Items.Remove(lst_mantenimiento.SelectedItem);
            MessageBox.Show("Datos eliminados");
        }

        private void lst_mantenimiento_Click(object sender, EventArgs e)
        {
            string textoSeleccionado;
            int longitudTexto;
            textoSeleccionado = lst_mantenimiento.SelectedItem.ToString().Trim();
            //MessageBox.Show(textoSeleccionado);
            longitudTexto = textoSeleccionado.Length;
            txt_codigo.Text = textoSeleccionado.Substring(0,3);
            txt_descripcion.Text = textoSeleccionado.Substring(4);
        }

        private void btn_actualizar_Click(object sender, EventArgs e)
        {
            estadoGuardar = 2;//actualiza registro
            lst_mantenimiento.Enabled = false;
            grb_mantenimiento.Enabled = true;
            txt_codigo.Enabled = false;
            grb_botonesPrincipales.Enabled = false;
            txt_descripcion.Focus();
        }
    }
}
