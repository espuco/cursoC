﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class Frm_ComboBox : Form
    {
        public Frm_ComboBox()
        {
            InitializeComponent();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            txt_Resultado.Text = "Curs seleccionado: " +cmb_CursoSeleccionado.SelectedItem +" posición: "+cmb_CursoSeleccionado.SelectedIndex;
        }

        private void btn_Registrar_Click(object sender, EventArgs e)
        {
            cmb_CursoSeleccionado.Items.Add(txt_Nuevo.Text);
            txt_Nuevo.Text = "";
            MessageBox.Show("Curso registrado   ");
        }
    }
}
