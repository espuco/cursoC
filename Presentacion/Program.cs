﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new Form1());
            //Application.Run(new frm_encuesta());
            //Application.Run(new Frm_ComboBox());
            //Application.Run(new Form2());
            //Application.Run(new Frm_tiempo());
            //Application.Run(new Frm_DataGridView());
            //Application.Run(new Frm_Principal());
            Application.Run(new FrmCategoria());
        }
    }
}
