﻿namespace Presentacion
{
    partial class FrmIvaCheckBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chb_JabaCajaCorporacionFavorita = new System.Windows.Forms.CheckBox();
            this.chb_JabaCajaHoreca = new System.Windows.Forms.CheckBox();
            this.chb_JabaCajaEmpleados = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.chb_UnidadEmpleado = new System.Windows.Forms.CheckBox();
            this.chb_UnidadHoreca = new System.Windows.Forms.CheckBox();
            this.chb_UnidadCorporacionFavorita = new System.Windows.Forms.CheckBox();
            this.txt_PrecioPorUnidadCorporacionFavorita = new System.Windows.Forms.TextBox();
            this.txt_PrecioPorUnidadHoreca = new System.Windows.Forms.TextBox();
            this.txt_PrecioPorUnidadEmpleado = new System.Windows.Forms.TextBox();
            this.chb_TieneIva = new System.Windows.Forms.CheckBox();
            this.txt_TieneIvaEmpleado = new System.Windows.Forms.TextBox();
            this.txt_TieneIvaHoreca = new System.Windows.Forms.TextBox();
            this.txt_TieneIvaCorporacionFavorita = new System.Windows.Forms.TextBox();
            this.txt_PrecioPorUnidadManejoEmpleado = new System.Windows.Forms.TextBox();
            this.txt_PrecioPorUnidadManejoHoreca = new System.Windows.Forms.TextBox();
            this.txt_PrecioPorUnidadManejoCorporacionFavorita = new System.Windows.Forms.TextBox();
            this.txt_PrecioPorUnidadFinalEmpleado = new System.Windows.Forms.TextBox();
            this.txt_PrecioPorUnidadFinalHoreca = new System.Windows.Forms.TextBox();
            this.txt_PrecioPorUnidadFinalCorporacionFavorita = new System.Windows.Forms.TextBox();
            this.txt_PrecioFinalEmpleado = new System.Windows.Forms.TextBox();
            this.txt_PrecioFinalHoreca = new System.Windows.Forms.TextBox();
            this.txt_PrecioFinalCorporacionFavorita = new System.Windows.Forms.TextBox();
            this.txt_IvaEmpleado = new System.Windows.Forms.TextBox();
            this.txt_IvaHoreca = new System.Windows.Forms.TextBox();
            this.txt_IvaCorporacionFavorita = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txt_UnidadDeManejo = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cmb_SistemaDeFacturacion = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // chb_JabaCajaCorporacionFavorita
            // 
            this.chb_JabaCajaCorporacionFavorita.AutoSize = true;
            this.chb_JabaCajaCorporacionFavorita.Enabled = false;
            this.chb_JabaCajaCorporacionFavorita.Location = new System.Drawing.Point(63, 61);
            this.chb_JabaCajaCorporacionFavorita.Name = "chb_JabaCajaCorporacionFavorita";
            this.chb_JabaCajaCorporacionFavorita.Size = new System.Drawing.Size(15, 14);
            this.chb_JabaCajaCorporacionFavorita.TabIndex = 0;
            this.chb_JabaCajaCorporacionFavorita.UseVisualStyleBackColor = true;
            // 
            // chb_JabaCajaHoreca
            // 
            this.chb_JabaCajaHoreca.AutoSize = true;
            this.chb_JabaCajaHoreca.Enabled = false;
            this.chb_JabaCajaHoreca.Location = new System.Drawing.Point(63, 84);
            this.chb_JabaCajaHoreca.Name = "chb_JabaCajaHoreca";
            this.chb_JabaCajaHoreca.Size = new System.Drawing.Size(15, 14);
            this.chb_JabaCajaHoreca.TabIndex = 1;
            this.chb_JabaCajaHoreca.UseVisualStyleBackColor = true;
            // 
            // chb_JabaCajaEmpleados
            // 
            this.chb_JabaCajaEmpleados.AutoSize = true;
            this.chb_JabaCajaEmpleados.Enabled = false;
            this.chb_JabaCajaEmpleados.Location = new System.Drawing.Point(63, 107);
            this.chb_JabaCajaEmpleados.Name = "chb_JabaCajaEmpleados";
            this.chb_JabaCajaEmpleados.Size = new System.Drawing.Size(15, 14);
            this.chb_JabaCajaEmpleados.TabIndex = 2;
            this.chb_JabaCajaEmpleados.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Favorita";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Horeca";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(1, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Empleados";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(42, 33);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Jaba/Caja";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(104, 33);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Unidad";
            // 
            // chb_UnidadEmpleado
            // 
            this.chb_UnidadEmpleado.AutoSize = true;
            this.chb_UnidadEmpleado.Enabled = false;
            this.chb_UnidadEmpleado.Location = new System.Drawing.Point(119, 107);
            this.chb_UnidadEmpleado.Name = "chb_UnidadEmpleado";
            this.chb_UnidadEmpleado.Size = new System.Drawing.Size(15, 14);
            this.chb_UnidadEmpleado.TabIndex = 9;
            this.chb_UnidadEmpleado.UseVisualStyleBackColor = true;
            this.chb_UnidadEmpleado.CheckedChanged += new System.EventHandler(this.chb_UnidadEmpleado_CheckedChanged);
            // 
            // chb_UnidadHoreca
            // 
            this.chb_UnidadHoreca.AutoSize = true;
            this.chb_UnidadHoreca.Enabled = false;
            this.chb_UnidadHoreca.Location = new System.Drawing.Point(119, 84);
            this.chb_UnidadHoreca.Name = "chb_UnidadHoreca";
            this.chb_UnidadHoreca.Size = new System.Drawing.Size(15, 14);
            this.chb_UnidadHoreca.TabIndex = 8;
            this.chb_UnidadHoreca.UseVisualStyleBackColor = true;
            // 
            // chb_UnidadCorporacionFavorita
            // 
            this.chb_UnidadCorporacionFavorita.AutoSize = true;
            this.chb_UnidadCorporacionFavorita.Enabled = false;
            this.chb_UnidadCorporacionFavorita.Location = new System.Drawing.Point(119, 61);
            this.chb_UnidadCorporacionFavorita.Name = "chb_UnidadCorporacionFavorita";
            this.chb_UnidadCorporacionFavorita.Size = new System.Drawing.Size(15, 14);
            this.chb_UnidadCorporacionFavorita.TabIndex = 7;
            this.chb_UnidadCorporacionFavorita.UseVisualStyleBackColor = true;
            // 
            // txt_PrecioPorUnidadCorporacionFavorita
            // 
            this.txt_PrecioPorUnidadCorporacionFavorita.Location = new System.Drawing.Point(163, 58);
            this.txt_PrecioPorUnidadCorporacionFavorita.Name = "txt_PrecioPorUnidadCorporacionFavorita";
            this.txt_PrecioPorUnidadCorporacionFavorita.Size = new System.Drawing.Size(70, 20);
            this.txt_PrecioPorUnidadCorporacionFavorita.TabIndex = 11;
            // 
            // txt_PrecioPorUnidadHoreca
            // 
            this.txt_PrecioPorUnidadHoreca.Location = new System.Drawing.Point(163, 84);
            this.txt_PrecioPorUnidadHoreca.Name = "txt_PrecioPorUnidadHoreca";
            this.txt_PrecioPorUnidadHoreca.Size = new System.Drawing.Size(70, 20);
            this.txt_PrecioPorUnidadHoreca.TabIndex = 12;
            // 
            // txt_PrecioPorUnidadEmpleado
            // 
            this.txt_PrecioPorUnidadEmpleado.Location = new System.Drawing.Point(163, 110);
            this.txt_PrecioPorUnidadEmpleado.Name = "txt_PrecioPorUnidadEmpleado";
            this.txt_PrecioPorUnidadEmpleado.Size = new System.Drawing.Size(70, 20);
            this.txt_PrecioPorUnidadEmpleado.TabIndex = 13;
            // 
            // chb_TieneIva
            // 
            this.chb_TieneIva.AutoSize = true;
            this.chb_TieneIva.Location = new System.Drawing.Point(239, 32);
            this.chb_TieneIva.Name = "chb_TieneIva";
            this.chb_TieneIva.Size = new System.Drawing.Size(73, 17);
            this.chb_TieneIva.TabIndex = 14;
            this.chb_TieneIva.Text = "Tiene IVA";
            this.chb_TieneIva.UseVisualStyleBackColor = true;
            this.chb_TieneIva.CheckedChanged += new System.EventHandler(this.chb_TieneIva_CheckedChanged);
            // 
            // txt_TieneIvaEmpleado
            // 
            this.txt_TieneIvaEmpleado.Location = new System.Drawing.Point(239, 109);
            this.txt_TieneIvaEmpleado.Name = "txt_TieneIvaEmpleado";
            this.txt_TieneIvaEmpleado.ReadOnly = true;
            this.txt_TieneIvaEmpleado.Size = new System.Drawing.Size(70, 20);
            this.txt_TieneIvaEmpleado.TabIndex = 17;
            this.txt_TieneIvaEmpleado.Visible = false;
            // 
            // txt_TieneIvaHoreca
            // 
            this.txt_TieneIvaHoreca.Location = new System.Drawing.Point(239, 83);
            this.txt_TieneIvaHoreca.Name = "txt_TieneIvaHoreca";
            this.txt_TieneIvaHoreca.ReadOnly = true;
            this.txt_TieneIvaHoreca.Size = new System.Drawing.Size(70, 20);
            this.txt_TieneIvaHoreca.TabIndex = 16;
            this.txt_TieneIvaHoreca.Visible = false;
            // 
            // txt_TieneIvaCorporacionFavorita
            // 
            this.txt_TieneIvaCorporacionFavorita.Location = new System.Drawing.Point(239, 57);
            this.txt_TieneIvaCorporacionFavorita.Name = "txt_TieneIvaCorporacionFavorita";
            this.txt_TieneIvaCorporacionFavorita.ReadOnly = true;
            this.txt_TieneIvaCorporacionFavorita.Size = new System.Drawing.Size(70, 20);
            this.txt_TieneIvaCorporacionFavorita.TabIndex = 15;
            this.txt_TieneIvaCorporacionFavorita.Visible = false;
            // 
            // txt_PrecioPorUnidadManejoEmpleado
            // 
            this.txt_PrecioPorUnidadManejoEmpleado.Location = new System.Drawing.Point(446, 107);
            this.txt_PrecioPorUnidadManejoEmpleado.Name = "txt_PrecioPorUnidadManejoEmpleado";
            this.txt_PrecioPorUnidadManejoEmpleado.ReadOnly = true;
            this.txt_PrecioPorUnidadManejoEmpleado.Size = new System.Drawing.Size(70, 20);
            this.txt_PrecioPorUnidadManejoEmpleado.TabIndex = 23;
            // 
            // txt_PrecioPorUnidadManejoHoreca
            // 
            this.txt_PrecioPorUnidadManejoHoreca.Location = new System.Drawing.Point(446, 81);
            this.txt_PrecioPorUnidadManejoHoreca.Name = "txt_PrecioPorUnidadManejoHoreca";
            this.txt_PrecioPorUnidadManejoHoreca.ReadOnly = true;
            this.txt_PrecioPorUnidadManejoHoreca.Size = new System.Drawing.Size(70, 20);
            this.txt_PrecioPorUnidadManejoHoreca.TabIndex = 22;
            // 
            // txt_PrecioPorUnidadManejoCorporacionFavorita
            // 
            this.txt_PrecioPorUnidadManejoCorporacionFavorita.Location = new System.Drawing.Point(446, 55);
            this.txt_PrecioPorUnidadManejoCorporacionFavorita.Name = "txt_PrecioPorUnidadManejoCorporacionFavorita";
            this.txt_PrecioPorUnidadManejoCorporacionFavorita.ReadOnly = true;
            this.txt_PrecioPorUnidadManejoCorporacionFavorita.Size = new System.Drawing.Size(70, 20);
            this.txt_PrecioPorUnidadManejoCorporacionFavorita.TabIndex = 21;
            // 
            // txt_PrecioPorUnidadFinalEmpleado
            // 
            this.txt_PrecioPorUnidadFinalEmpleado.Location = new System.Drawing.Point(332, 107);
            this.txt_PrecioPorUnidadFinalEmpleado.Name = "txt_PrecioPorUnidadFinalEmpleado";
            this.txt_PrecioPorUnidadFinalEmpleado.ReadOnly = true;
            this.txt_PrecioPorUnidadFinalEmpleado.Size = new System.Drawing.Size(70, 20);
            this.txt_PrecioPorUnidadFinalEmpleado.TabIndex = 20;
            // 
            // txt_PrecioPorUnidadFinalHoreca
            // 
            this.txt_PrecioPorUnidadFinalHoreca.Location = new System.Drawing.Point(332, 81);
            this.txt_PrecioPorUnidadFinalHoreca.Name = "txt_PrecioPorUnidadFinalHoreca";
            this.txt_PrecioPorUnidadFinalHoreca.ReadOnly = true;
            this.txt_PrecioPorUnidadFinalHoreca.Size = new System.Drawing.Size(70, 20);
            this.txt_PrecioPorUnidadFinalHoreca.TabIndex = 19;
            // 
            // txt_PrecioPorUnidadFinalCorporacionFavorita
            // 
            this.txt_PrecioPorUnidadFinalCorporacionFavorita.Location = new System.Drawing.Point(332, 55);
            this.txt_PrecioPorUnidadFinalCorporacionFavorita.Name = "txt_PrecioPorUnidadFinalCorporacionFavorita";
            this.txt_PrecioPorUnidadFinalCorporacionFavorita.ReadOnly = true;
            this.txt_PrecioPorUnidadFinalCorporacionFavorita.Size = new System.Drawing.Size(70, 20);
            this.txt_PrecioPorUnidadFinalCorporacionFavorita.TabIndex = 18;
            // 
            // txt_PrecioFinalEmpleado
            // 
            this.txt_PrecioFinalEmpleado.Location = new System.Drawing.Point(620, 107);
            this.txt_PrecioFinalEmpleado.Name = "txt_PrecioFinalEmpleado";
            this.txt_PrecioFinalEmpleado.ReadOnly = true;
            this.txt_PrecioFinalEmpleado.Size = new System.Drawing.Size(70, 20);
            this.txt_PrecioFinalEmpleado.TabIndex = 29;
            // 
            // txt_PrecioFinalHoreca
            // 
            this.txt_PrecioFinalHoreca.Location = new System.Drawing.Point(620, 81);
            this.txt_PrecioFinalHoreca.Name = "txt_PrecioFinalHoreca";
            this.txt_PrecioFinalHoreca.ReadOnly = true;
            this.txt_PrecioFinalHoreca.Size = new System.Drawing.Size(70, 20);
            this.txt_PrecioFinalHoreca.TabIndex = 28;
            // 
            // txt_PrecioFinalCorporacionFavorita
            // 
            this.txt_PrecioFinalCorporacionFavorita.Location = new System.Drawing.Point(620, 55);
            this.txt_PrecioFinalCorporacionFavorita.Name = "txt_PrecioFinalCorporacionFavorita";
            this.txt_PrecioFinalCorporacionFavorita.ReadOnly = true;
            this.txt_PrecioFinalCorporacionFavorita.Size = new System.Drawing.Size(70, 20);
            this.txt_PrecioFinalCorporacionFavorita.TabIndex = 27;
            // 
            // txt_IvaEmpleado
            // 
            this.txt_IvaEmpleado.Location = new System.Drawing.Point(544, 107);
            this.txt_IvaEmpleado.Name = "txt_IvaEmpleado";
            this.txt_IvaEmpleado.ReadOnly = true;
            this.txt_IvaEmpleado.Size = new System.Drawing.Size(70, 20);
            this.txt_IvaEmpleado.TabIndex = 26;
            // 
            // txt_IvaHoreca
            // 
            this.txt_IvaHoreca.Location = new System.Drawing.Point(544, 81);
            this.txt_IvaHoreca.Name = "txt_IvaHoreca";
            this.txt_IvaHoreca.ReadOnly = true;
            this.txt_IvaHoreca.Size = new System.Drawing.Size(70, 20);
            this.txt_IvaHoreca.TabIndex = 25;
            // 
            // txt_IvaCorporacionFavorita
            // 
            this.txt_IvaCorporacionFavorita.Location = new System.Drawing.Point(544, 55);
            this.txt_IvaCorporacionFavorita.Name = "txt_IvaCorporacionFavorita";
            this.txt_IvaCorporacionFavorita.ReadOnly = true;
            this.txt_IvaCorporacionFavorita.Size = new System.Drawing.Size(70, 20);
            this.txt_IvaCorporacionFavorita.TabIndex = 24;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(151, 33);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 13);
            this.label6.TabIndex = 30;
            this.label6.Text = "Precio x Unidad";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(312, 33);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(107, 13);
            this.label7.TabIndex = 31;
            this.label7.Text = "Precio x Unidad Final";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(425, 32);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(120, 13);
            this.label8.TabIndex = 32;
            this.label8.Text = "Precio x Unidad Manejo";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(564, 32);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(24, 13);
            this.label9.TabIndex = 33;
            this.label9.Text = "IVA";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(615, 32);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(62, 13);
            this.label10.TabIndex = 34;
            this.label10.Text = "Precio Final";
            // 
            // txt_UnidadDeManejo
            // 
            this.txt_UnidadDeManejo.Location = new System.Drawing.Point(366, 177);
            this.txt_UnidadDeManejo.Name = "txt_UnidadDeManejo";
            this.txt_UnidadDeManejo.Size = new System.Drawing.Size(100, 20);
            this.txt_UnidadDeManejo.TabIndex = 35;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(250, 180);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(94, 13);
            this.label11.TabIndex = 36;
            this.label11.Text = "Unidad de Manejo";
            // 
            // cmb_SistemaDeFacturacion
            // 
            this.cmb_SistemaDeFacturacion.FormattingEnabled = true;
            this.cmb_SistemaDeFacturacion.Items.AddRange(new object[] {
            "Jaba",
            "Caja",
            "Unidad"});
            this.cmb_SistemaDeFacturacion.Location = new System.Drawing.Point(123, 176);
            this.cmb_SistemaDeFacturacion.Name = "cmb_SistemaDeFacturacion";
            this.cmb_SistemaDeFacturacion.Size = new System.Drawing.Size(121, 21);
            this.cmb_SistemaDeFacturacion.TabIndex = 37;
            this.cmb_SistemaDeFacturacion.SelectedIndexChanged += new System.EventHandler(this.cmb_SistemaDeFacturacion_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(1, 179);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(118, 13);
            this.label12.TabIndex = 38;
            this.label12.Text = "Sistema de Facturación";
            // 
            // FrmIvaCheckBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(697, 208);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.cmb_SistemaDeFacturacion);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txt_UnidadDeManejo);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txt_PrecioFinalEmpleado);
            this.Controls.Add(this.txt_PrecioFinalHoreca);
            this.Controls.Add(this.txt_PrecioFinalCorporacionFavorita);
            this.Controls.Add(this.txt_IvaEmpleado);
            this.Controls.Add(this.txt_IvaHoreca);
            this.Controls.Add(this.txt_IvaCorporacionFavorita);
            this.Controls.Add(this.txt_PrecioPorUnidadManejoEmpleado);
            this.Controls.Add(this.txt_PrecioPorUnidadManejoHoreca);
            this.Controls.Add(this.txt_PrecioPorUnidadManejoCorporacionFavorita);
            this.Controls.Add(this.txt_PrecioPorUnidadFinalEmpleado);
            this.Controls.Add(this.txt_PrecioPorUnidadFinalHoreca);
            this.Controls.Add(this.txt_PrecioPorUnidadFinalCorporacionFavorita);
            this.Controls.Add(this.txt_TieneIvaEmpleado);
            this.Controls.Add(this.txt_TieneIvaHoreca);
            this.Controls.Add(this.txt_TieneIvaCorporacionFavorita);
            this.Controls.Add(this.chb_TieneIva);
            this.Controls.Add(this.txt_PrecioPorUnidadEmpleado);
            this.Controls.Add(this.txt_PrecioPorUnidadHoreca);
            this.Controls.Add(this.txt_PrecioPorUnidadCorporacionFavorita);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.chb_UnidadEmpleado);
            this.Controls.Add(this.chb_UnidadHoreca);
            this.Controls.Add(this.chb_UnidadCorporacionFavorita);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chb_JabaCajaEmpleados);
            this.Controls.Add(this.chb_JabaCajaHoreca);
            this.Controls.Add(this.chb_JabaCajaCorporacionFavorita);
            this.Name = "FrmIvaCheckBox";
            this.Text = "FrmIvaCheckBox";
            this.Load += new System.EventHandler(this.FrmIvaCheckBox_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chb_JabaCajaCorporacionFavorita;
        private System.Windows.Forms.CheckBox chb_JabaCajaHoreca;
        private System.Windows.Forms.CheckBox chb_JabaCajaEmpleados;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox chb_UnidadEmpleado;
        private System.Windows.Forms.CheckBox chb_UnidadHoreca;
        private System.Windows.Forms.CheckBox chb_UnidadCorporacionFavorita;
        private System.Windows.Forms.TextBox txt_PrecioPorUnidadCorporacionFavorita;
        private System.Windows.Forms.TextBox txt_PrecioPorUnidadHoreca;
        private System.Windows.Forms.TextBox txt_PrecioPorUnidadEmpleado;
        private System.Windows.Forms.CheckBox chb_TieneIva;
        private System.Windows.Forms.TextBox txt_TieneIvaEmpleado;
        private System.Windows.Forms.TextBox txt_TieneIvaHoreca;
        private System.Windows.Forms.TextBox txt_TieneIvaCorporacionFavorita;
        private System.Windows.Forms.TextBox txt_PrecioPorUnidadManejoEmpleado;
        private System.Windows.Forms.TextBox txt_PrecioPorUnidadManejoHoreca;
        private System.Windows.Forms.TextBox txt_PrecioPorUnidadManejoCorporacionFavorita;
        private System.Windows.Forms.TextBox txt_PrecioPorUnidadFinalEmpleado;
        private System.Windows.Forms.TextBox txt_PrecioPorUnidadFinalHoreca;
        private System.Windows.Forms.TextBox txt_PrecioPorUnidadFinalCorporacionFavorita;
        private System.Windows.Forms.TextBox txt_PrecioFinalEmpleado;
        private System.Windows.Forms.TextBox txt_PrecioFinalHoreca;
        private System.Windows.Forms.TextBox txt_PrecioFinalCorporacionFavorita;
        private System.Windows.Forms.TextBox txt_IvaEmpleado;
        private System.Windows.Forms.TextBox txt_IvaHoreca;
        private System.Windows.Forms.TextBox txt_IvaCorporacionFavorita;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txt_UnidadDeManejo;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cmb_SistemaDeFacturacion;
        private System.Windows.Forms.Label label12;
    }
}