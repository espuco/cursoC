﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocio;
using Entidades;
namespace Presentacion
{
    public partial class FrmCategoria : Form
    {
        public FrmCategoria()
        {
            InitializeComponent();
        }

        #region "mis variables"
        int EstadoGuardar = 0;//sin ninguna opción (1 nuevo 2 edita)
        int NumeroCodigo = 0;// guarda el código del registro seleccionado
        #endregion "mis variables"
        #region "mis métodos"
        private void ListarCategoria(string codigo)
        {//recibe la información
            DW_Categoria.DataSource = CN_Categoria.ListarCategoria(codigo);
            //FormatoCategoria();
        }
        private void FormatoCategoria()
        {
            DW_Categoria.Columns[0].Width = 80;
            DW_Categoria.Columns[0].HeaderText = "CÓDIGO";
            DW_Categoria.Columns[1].Width = 250;
            DW_Categoria.Columns[1].HeaderText = "DESCRIPCIÓN";
        }

        private void Estado(bool Estado)
        {
            txt_descripcion.Enabled = Estado;
            btn_Cancelar.Enabled = Estado;
            btn_Guardar.Enabled = Estado;

            btn_Nuevo.Enabled = !Estado;
            btn_Actualizar.Enabled = !Estado;
            btn_Eliminar.Enabled = !Estado;
            btn_Reporte.Enabled = !Estado;
            btn_Salir.Enabled = !Estado;

            txt_Buscar.Enabled = !Estado;
            btn_Buscar.Enabled = !Estado;
            DW_Categoria.Enabled = !Estado;
        }
        #endregion "mis métodos"

        private void FrmCategoria_Load(object sender, EventArgs e)
        {//carga la información en el datagridview "%"significa 
            this.ListarCategoria("%");
        }

        private void btn_Guardar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txt_descripcion.Text))
            {
                MessageBox.Show("No se tiene información para ser guardada", "Aviso  del sistema", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                string Respuesta = "";
                E_Categoria Categoria = new E_Categoria();
                Categoria.codigo = NumeroCodigo;
                Categoria.descripcion = txt_descripcion.Text.Trim();
                Respuesta = CN_Categoria.GuardarCategoria(EstadoGuardar, Categoria);
                if (Respuesta.Equals("OK"))
                {
                    this.ListarCategoria("%");
                    Estado(false);
                    EstadoGuardar = 0;//ya concluye el proceso de guardar
                    NumeroCodigo = 0;// no es es necesario seguir almacenando el valor
                    MessageBox.Show("Dtos guardados", "Aviso del sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                    MessageBox.Show(Respuesta, "Aviso del sistema", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btn_Buscar_Click(object sender, EventArgs e)
        {
            this.ListarCategoria(txt_Buscar.Text.Trim());
        }

        private void btn_Nuevo_Click(object sender, EventArgs e)
        {
            EstadoGuardar = 1; //nuevo registro
            this.Estado(true);
            txt_descripcion.Text = "";
        }

        private void btn_Cancelar_Click(object sender, EventArgs e)
        {
            Estado(false);
            txt_descripcion.Text = "";
        }

        private void btn_Actualizar_Click(object sender, EventArgs e)
        {
            EstadoGuardar = 2;//actualizar registro
            Estado(true);
        }

        private void DW_Categoria_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //información de fila posicionado: CurrentRow   
            //informacion de la celda: Cells[""]
            //NumeroCodigo = Convert.ToInt32(DW_Categoria.CurrentRow.Cells["codigo_ca"].Value);
            //txt_descripcion.Text = Convert.ToString(DW_Categoria.CurrentRow.Cells["descripcion_ca"].Value);
            NumeroCodigo = Convert.ToInt32(DW_Categoria.CurrentRow.Cells[0].Value);
            txt_descripcion.Text = Convert.ToString(DW_Categoria.CurrentRow.Cells[1].Value);
        }

        private void btn_Eliminar_Click(object sender, EventArgs e)
        {
            DialogResult opcion;
            opcion = MessageBox.Show("¿Seguro de eliminar?", "Aviso del sistema", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (opcion == DialogResult.Yes)
            {
                string respuesta = "";
                respuesta = CN_Categoria.EliminarCategoria(NumeroCodigo);
                if (respuesta.Equals("OK"))
                {
                    this.ListarCategoria("%");
                    NumeroCodigo = 0;// no es es necesario seguir almacenando el valor
                    txt_descripcion.Text = "";
                    MessageBox.Show("Registro eliminado", "Aviso del sistema", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    MessageBox.Show(respuesta, "Aviso del sistema", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox1.Text)&&string.IsNullOrEmpty(textBox2.Text))
            {
                MessageBox.Show("faltan");return;
            }
            //int Estado = 0;
            //if (CheckTextBox(textBox1) == true || CheckTextBox(textBox2) == true)
            //{
            //    Estado = 1;
            //    return;
            //}
            //limpia cualquier mensaje de error de alguna corrida previa
            //BorrarMesaje();
            ////llamamos al método para validar campos, el de nombre y apellido
            //if (validarCampos())
            //{
            //    MessageBox.Show("Los datos se ingresaron correctamente");
            //}
        }
        private bool CheckTextBox(TextBox tb)
        {
            if (string.IsNullOrEmpty(tb.Text))
            {
                MessageBox.Show(tb.Name + "must be filled");
                return false;
            }
            else return true;
        }

        private bool validarCampos()
        {
            //variable que verifica si algo ha sido validado
            bool validado = true;
            if (textBox1.Text == "") //vefica que no quede vacío el campo
            {
                validado = false; //si está vacío validado es falso
                errorProvider1.SetError(textBox1, "Ingresar nombre"); 
                //por lo tanto manda a llamar a errorprovider en los parámetros de setError se identifica a quién estoy validando y el mensaje que deseo mandar
            }
            //verifico la casilla de apellido
            if (textBox2.Text == "")
            {
                validado = false;
                //digo que verifico a txtapellido y si no cumple mando ese mensaje
                errorProvider1.SetError(textBox2, "Ingrese apellido");
            }
            return validado;
        }
        private void BorrarMesaje()
        {
            //borra los mensajes para que no se muestren y pueda limpiar
            errorProvider1.SetError(textBox1, "");
            errorProvider1.SetError(textBox2, "");
        }

        private void textBox2_Validating(object sender, CancelEventArgs e)
        {
            int num;
            if (!int.TryParse(textBox2.Text,out num))
            {
                errorProvider1.SetError(textBox2, "Ingrese apellido");
            }
            else
            {
                errorProvider1.SetError(textBox2, "");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("nuevo deploy","mensaje del sistema",MessageBoxButtons.OK,MessageBoxIcon.Information);
        }
    }
}
