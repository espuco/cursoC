﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class Frm_tiempo : Form
    {
        public Frm_tiempo()
        {
            InitializeComponent();
        }
        int Contador = 0;

        private void timer1_Tick(object sender, EventArgs e)
        {
            //if (Contador < 100)
            //{
            //    Contador++;
            //    txt_contador.Text = Convert.ToString(Contador);
            //    //progressBar1.Value=Contador;
            //    progressBar1.Increment(1);
            //}
            //else
            //{
            //    timer1.Stop();
            //}
            if (progressBar1.Value < 100)
            {
                progressBar1.Increment(1);
                txt_contador.Text = Convert.ToString(progressBar1.Value);
            } else
                timer1.Stop();
            
        }

        private void btn_iniciar_Click(object sender, EventArgs e)
        {
            timer1.Start();
        }

        private void btn_parar_Click(object sender, EventArgs e)
        {
            timer1.Stop();
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            lbl_Porcentaje.Text = "Porcentaje:  " + trackBar1.Value.ToString()+ " %";
        }
    }
}
