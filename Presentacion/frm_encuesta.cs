﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class frm_encuesta : Form
    {
        public frm_encuesta()
        {
            InitializeComponent();
        }

        private void btn_procesar_Click(object sender, EventArgs e)
        {
            string resultado = "";

            if (ch_foxPro.Checked == true)
            {
                resultado += ch_foxPro.Text+", ";
            }
            if (ch_C.Checked == true)
            {
                resultado += ch_C.Text + ", ";
            }
            if (ch_visualBasic.Checked == true)
            {
                resultado += ch_visualBasic.Text + ", ";
            }
            if (ch_java.Checked == true)
            {
                resultado += ch_java.Text;
            }
            if (rb_presencial.Checked == true)
                resultado += rb_presencial.Text+"\n";
            else if (rb_virtual.Checked == true)
                resultado += rb_virtual.Text + "\n";
            txt_resultado.Text = resultado;
        }
    }
}
