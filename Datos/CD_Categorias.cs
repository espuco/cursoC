﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using Entidades;

namespace Datos
{
    public class CD_Categorias
    {
        public DataTable ListarCategoria(string codigo)
        {
            SqlDataReader Resultado;
            DataTable MiTabla = new DataTable();
            SqlConnection SqlConnection = new SqlConnection();
            try
            {
                SqlConnection = Conexion.GetInstancia().CrearConexion();
                SqlCommand Comando = new SqlCommand("PA_LISTADO_CATEGORIA", SqlConnection);
                Comando.CommandType = CommandType.StoredProcedure;
                Comando.Parameters.Add("@codigo", SqlDbType.VarChar).Value = codigo;
                //Comando.Parameters.AddWithValue("@codigo", codigo);
                SqlConnection.Open();
                Resultado = Comando.ExecuteReader();
                MiTabla.Load(Resultado);
                return MiTabla;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (SqlConnection.State == ConnectionState.Open) SqlConnection.Close();
            }
        }

        public string GuardarCategoria(int opcion, E_Categoria categoria)
        {
            string respuesta = "";
            SqlConnection SqlConnection = new SqlConnection();
            try
            {
                SqlConnection = Conexion.GetInstancia().CrearConexion();
                SqlCommand Comando = new SqlCommand("PA_GUARDAR_CATEGORIA", SqlConnection);
                Comando.CommandType = CommandType.StoredProcedure;
                Comando.Parameters.Add("@opcion", SqlDbType.Int).Value = opcion;
                Comando.Parameters.Add("@codigo", SqlDbType.Int).Value = categoria.codigo;
                Comando.Parameters.Add("@descripcion", SqlDbType.VarChar).Value = categoria.descripcion;
                //Comando.Parameters.AddWithValue("@opcion", opcion);
                //Comando.Parameters.AddWithValue("@codigo", categoria.codigo);
                //Comando.Parameters.AddWithValue("@descripcion", categoria.descripcion);
                SqlConnection.Open();
                respuesta = Comando.ExecuteNonQuery() == 1 ? "OK" : "No se pudo registar el proceso";
            }
            catch (Exception ex)
            {

                respuesta = ex.Message;
            }
            finally
            {
                if (SqlConnection.State == ConnectionState.Open) SqlConnection.Close();
            }
            return respuesta;
        }

        public string EliminarCategoria(int codigo)
        {
            string respuesta = "";
            SqlConnection SqlConnection = new SqlConnection();
            try
            {
                SqlConnection = Conexion.GetInstancia().CrearConexion();
                SqlCommand Comando = new SqlCommand("PA_ELIMINAR_CATEGORIA", SqlConnection);
                Comando.CommandType = CommandType.StoredProcedure;
                //Comando.Parameters.Add("@codigoEliminar", SqlDbType.Int).Value = codigo;            
                Comando.Parameters.AddWithValue("@codigoEliminar", codigo);                
                SqlConnection.Open();
                respuesta = Comando.ExecuteNonQuery() == 1 ? "OK" : "No se pudo eliminar el registro";
            }
            catch (Exception ex)
            {

                respuesta = ex.Message;
            }
            finally
            {
                if (SqlConnection.State == ConnectionState.Open) SqlConnection.Close();
            }
            return respuesta;
        }
    }
}
